package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.DispatchDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class DisToDto implements Function<Dispatch, DispatchDto> {
    @Override
    public DispatchDto apply(Dispatch dispatch) {
        DispatchDto dispatchDto = new DispatchDto();
        dispatchDto.setId(dispatch.getId());
        dispatchDto.setDangerLevel(dispatch.getDangerLevel());
        dispatchDto.setType(dispatch.getType());
        dispatchDto.setStart(dispatch.getDispatchStart());
        return dispatchDto;
    }
}

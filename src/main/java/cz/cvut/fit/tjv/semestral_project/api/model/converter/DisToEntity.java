package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.DispatchDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class DisToEntity implements Function<DispatchDto, Dispatch> {
    @Override
    public Dispatch apply(DispatchDto dispatchDto) {
        Dispatch dispatch = new Dispatch();
        dispatch.setId(dispatchDto.getId());
        dispatch.setDangerLevel(dispatchDto.getDangerLevel());
        dispatch.setType(dispatchDto.getType());
        dispatch.setDispatchStart(dispatchDto.getStart());
        return dispatch;
    }
}

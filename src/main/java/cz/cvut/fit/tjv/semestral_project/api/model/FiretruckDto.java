package cz.cvut.fit.tjv.semestral_project.api.model;

import cz.cvut.fit.tjv.semestral_project.domain.enums.FiretruckType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class FiretruckDto extends DtoWithId<Long> {
    @Size(min = 3, max = 255)
    private String name;
    @NotNull
    private FiretruckType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FiretruckType getType() {
        return type;
    }

    public void setType(FiretruckType type) {
        this.type = type;
    }
}

package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class DepToEntity implements Function<DepartmentDto, Department> {
    @Override
    public Department apply(DepartmentDto aDepartmentDto) {
        Department department = new Department();
        department.setId(aDepartmentDto.getId());
        department.setName(aDepartmentDto.getName());
        department.setLevel(aDepartmentDto.getLevel());
        return department;
    }
}

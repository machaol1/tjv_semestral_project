package cz.cvut.fit.tjv.semestral_project.repository;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FiretruckRepository extends CrudRepository<Firetruck, Long> {
}

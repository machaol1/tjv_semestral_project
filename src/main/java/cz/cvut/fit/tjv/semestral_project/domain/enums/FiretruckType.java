package cz.cvut.fit.tjv.semestral_project.domain.enums;

public enum FiretruckType {
    DA, CISTERNA, OSOBNI
}

package cz.cvut.fit.tjv.semestral_project.api.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.LocalDateTime;

public class DispatchDto extends DtoWithId<Long> {
    @Min(1)
    @Max(7)
    private Integer dangerLevel;
    @NotNull
    @Size(min = 3, max = 255)
    private String type;
    @NotNull
    private LocalDateTime start;

    public Integer getDangerLevel() {
        return dangerLevel;
    }

    public void setDangerLevel(Integer dangerLevel) {
        this.dangerLevel = dangerLevel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }
}

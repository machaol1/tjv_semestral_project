package cz.cvut.fit.tjv.semestral_project.api.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class DepartmentDto extends DtoWithId<Long> {
    @NotNull
    @Size(min = 3, max = 255)
    private String name;
    @NotNull
    @Min(1)
    @Max(6)
    private Integer level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}

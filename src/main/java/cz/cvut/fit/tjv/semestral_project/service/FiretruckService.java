package cz.cvut.fit.tjv.semestral_project.service;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FiretruckRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class FiretruckService extends AbstractCrudService<Firetruck, Long> {
    private final DepartmentRepository departmentRepository;

    public FiretruckService(FiretruckRepository firetruckRepository,
                            DepartmentRepository departmentRepository) {
        super(firetruckRepository);
        this.departmentRepository = departmentRepository;
    }

    public void setDepartment(Long idFT, Long idDep) {
        Firetruck firetruck = findOrThrow(idFT);
        Department department = departmentRepository.findById(idDep).orElseThrow();

        if (firetruck.getDepartment() != null) {
            firetruck.getDepartment().removeFiretruck(firetruck);
            departmentRepository.save(firetruck.getDepartment());
        }

        firetruck.setDepartment(department);
        department.removeFiretruck(firetruck);

        repository.save(firetruck);
        departmentRepository.save(department);
    }

    @Override
    public void update(Firetruck e) throws NoSuchElementException, IllegalArgumentException {
        validate(e);
        Firetruck oldFiretruck = findOrThrow(e.getId());
        e.setDepartment(oldFiretruck.getDepartment());
        super.update(e);
    }

    @Override
    public void deleteById(Long id) throws NoSuchElementException {
        Firetruck firetruck = findOrThrow(id);

        // Remove the reference from the department
        if (firetruck.getDepartment() != null) {
            firetruck.getDepartment().removeFiretruck(firetruck);
            firetruck.setDepartment(null);
        }

        super.deleteById(id);
    }

    @Override
    protected void validate(Firetruck firetruck) throws IllegalArgumentException {
        if (firetruck.getType() == null
        || firetruck.getName() != null
            && ( firetruck.getName().length() < 3
            || firetruck.getName().length() > 255 ) )
            throw new IllegalArgumentException();
    }
}

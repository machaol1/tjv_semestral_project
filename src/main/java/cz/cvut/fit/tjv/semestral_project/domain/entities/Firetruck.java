package cz.cvut.fit.tjv.semestral_project.domain.entities;

import cz.cvut.fit.tjv.semestral_project.domain.enums.FiretruckType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

@Entity
public class Firetruck extends EntityWithId<Long> {
    private String name;
    @Column(nullable = false)
    private FiretruckType type;
    @ManyToOne
    private Department department;

    public FiretruckType getType() {
        return type;
    }

    public void setType(FiretruckType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Dispatch{" +
                "id=" + id +
                ", name=" + name +
                ", type=" + type +
                ", department=" + department +
                '}';
    }
}

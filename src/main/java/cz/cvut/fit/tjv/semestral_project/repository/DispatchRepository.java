package cz.cvut.fit.tjv.semestral_project.repository;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DispatchRepository extends CrudRepository<Dispatch, Long> {
}

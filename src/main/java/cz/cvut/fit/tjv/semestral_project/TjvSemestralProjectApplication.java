package cz.cvut.fit.tjv.semestral_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TjvSemestralProjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(TjvSemestralProjectApplication.class, args);
    }
}

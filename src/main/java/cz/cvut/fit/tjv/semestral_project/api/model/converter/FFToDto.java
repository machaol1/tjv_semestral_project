package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.FirefighterDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FFToDto implements Function<Firefighter, FirefighterDto> {
    @Override
    public FirefighterDto apply(Firefighter firefighter) {
        FirefighterDto firefighterDto = new FirefighterDto();
        firefighterDto.setId(firefighter.getId());
        firefighterDto.setName(firefighter.getName());
        firefighterDto.setPhoneNumber(firefighter.getPhoneNumber());
        firefighterDto.setBirthDate(firefighter.getBirthDate());
        return firefighterDto;
    }
}

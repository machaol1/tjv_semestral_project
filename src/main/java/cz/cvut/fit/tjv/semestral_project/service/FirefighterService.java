package cz.cvut.fit.tjv.semestral_project.service;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import cz.cvut.fit.tjv.semestral_project.repository.DispatchRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FirefighterRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.NoSuchElementException;

@Service
public class FirefighterService extends AbstractCrudService<Firefighter, Long> {
    private final DispatchRepository dispatchRepository;
    private final DepartmentRepository departmentRepository;

    public FirefighterService(FirefighterRepository firefighterRepository,
                              DispatchRepository dispatchRepository,
                              DepartmentRepository departmentRepository) {
        super(firefighterRepository);
        this.dispatchRepository = dispatchRepository;
        this.departmentRepository = departmentRepository;
    }

    public void attend(Long idFF, Long idDis) throws NoSuchElementException,
            FirefighterCannotAttendDispatchHisDepartmentDidNotException {
        Firefighter firefighter = findOrThrow(idFF);
        Dispatch dispatch = dispatchRepository.findById(idDis).orElseThrow();

        if (!dispatch.getDepartments().contains(firefighter.getDepartment()))
            throw new FirefighterCannotAttendDispatchHisDepartmentDidNotException();

        firefighter.addDispatch(dispatch);
        dispatch.addAttendee(firefighter);

        dispatchRepository.save(dispatch);
        repository.save(firefighter);
    }

    public void removeAttend(Long idFF, Long idDis) throws NoSuchElementException {
        Firefighter firefighter = findOrThrow(idFF);
        Dispatch dispatch = dispatchRepository.findById(idDis).orElseThrow();

        firefighter.removeDispatch(dispatch);
        dispatch.removeAttendee(firefighter);

        dispatchRepository.save(dispatch);
        repository.save(firefighter);
    }

    public void setDepartment(Long idFF, Long idDep) {
        Firefighter firefighter = findOrThrow(idFF);
        Department department = departmentRepository.findById(idDep).orElseThrow();

        if (firefighter.getDepartment() != null) {
            firefighter.getDepartment().removeFirefighter(firefighter);
            departmentRepository.save(firefighter.getDepartment());
        }

        firefighter.setDepartment(department);
        department.addFirefighter(firefighter);

        repository.save(firefighter);
        departmentRepository.save(department);
    }

    @Override
    public void update(Firefighter e) throws NoSuchElementException, IllegalArgumentException {
        validate(e);
        Firefighter oldFirefighter = findOrThrow(e.getId());
        // could be improved
        oldFirefighter.getDispatches().forEach(e::addDispatch);
        e.setDepartment(oldFirefighter.getDepartment());

        super.update(e);
    }

    @Override
    public void deleteById(Long id) throws NoSuchElementException {
        Firefighter firefighter = findOrThrow(id);

        // Remove associations from dispatches
        firefighter.getDispatches().forEach(dispatch -> dispatch.removeAttendee(firefighter));
        dispatchRepository.saveAll(firefighter.getDispatches());

        // Remove association from department
        if (firefighter.getDepartment() != null) {
            firefighter.getDepartment().removeFirefighter(firefighter);
            firefighter.setDepartment(null);
        }

        super.deleteById(id);
    }

    @Override
    protected void validate(Firefighter firefighter) throws IllegalArgumentException {
        if (firefighter.getName() == null || firefighter.getBirthDate() == null
        || Period.between(firefighter.getBirthDate(), LocalDate.now()).getYears() < 18 )
            throw new IllegalArgumentException();
    }
}

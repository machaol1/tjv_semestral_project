package cz.cvut.fit.tjv.semestral_project.api.model;

public abstract class DtoWithId<ID> {
    protected ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}

package cz.cvut.fit.tjv.semestral_project.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;

import java.time.LocalDate;
import java.util.*;

@Entity
public class Firefighter extends EntityWithId<Long> {
    @Column(nullable = false, length = 48)
    private String name;
    private String phoneNumber;
    @Column(nullable = false)
    private LocalDate birthDate;
    @ManyToOne
    private Department department;
    @ManyToMany
    private Set<Dispatch> dispatches = new HashSet<>();

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String fullName) {
        this.name = fullName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Collection<Dispatch> getDispatches() {
        return Collections.unmodifiableCollection(dispatches);
    }

    public void addDispatch(Dispatch dispatch) {
        dispatches.add(Objects.requireNonNull(dispatch));
    }

    public void removeDispatch(Dispatch dispatch) {
        dispatches.remove(dispatch);
    }

    @Override
    public String toString() {
        return "Firefighter{" +
                "id=" + id +
                ", name=" + name +
                ", textContents=" + phoneNumber +
                ", department=" + department +
                ", dispatches=" + dispatches +
                '}';
    }
}

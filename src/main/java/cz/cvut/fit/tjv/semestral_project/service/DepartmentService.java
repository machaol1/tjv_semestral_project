package cz.cvut.fit.tjv.semestral_project.service;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import cz.cvut.fit.tjv.semestral_project.repository.DispatchRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FirefighterRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FiretruckRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.NoSuchElementException;

@Service
public class DepartmentService extends AbstractCrudService<Department, Long> {
    private final FirefighterRepository firefighterRepository;
    private final FirefighterService firefighterService;
    private final DispatchRepository dispatchRepository;
    private final FiretruckRepository firetruckRepository;
    private final FiretruckService firetruckService;

    public DepartmentService(DepartmentRepository departmentRepository,
                             FirefighterRepository firefighterRepository,
                             DispatchRepository dispatchRepository,
                             FiretruckRepository firetruckRepository,
                             FirefighterService firefighterService,
                             FiretruckService firetruckService) {
        super(departmentRepository);
        this.firefighterRepository = firefighterRepository;
        this.dispatchRepository = dispatchRepository;
        this.firetruckRepository = firetruckRepository;
        this.firefighterService = firefighterService;
        this.firetruckService = firetruckService;
    }

    public void moveFiretruck(Long idDep, Long idFT) throws NoSuchElementException {
        Department department = findOrThrow(idDep);
        Firetruck firetruck = firetruckService.findOrThrow(idFT);
        if (firetruck.getDepartment() != null) {
            firetruck.getDepartment().removeFiretruck(firetruck); // Remove firetruck from previous owner
            repository.save(firetruck.getDepartment());
        }
        firetruck.setDepartment(department);
        department.addFiretruck(firetruck);
        firetruckRepository.save(firetruck);
        repository.save(department);
    }

    public Firetruck newFiretruck(Long id, Firetruck e) throws IllegalArgumentException, NoSuchElementException {
        Department department = findOrThrow(id);

        e.setDepartment(department);
        Firetruck ret = firetruckService.create(e);

        department.addFiretruck(e);

        repository.save(department);
        return ret;
    }

    public void addDispatch(Long idDep, Long idDis) throws NoSuchElementException {
        Department department = findOrThrow(idDep);
        Dispatch dispatch = dispatchRepository.findById(idDis).orElseThrow();
        dispatch.addDepartment(department);
        department.addDispatch(dispatch);
        dispatchRepository.save(dispatch);
        repository.save(department);
    }

    public void removeDispatch(Long idDep, Long idDis) throws NoSuchElementException {
        Department department = findOrThrow(idDep);
        Dispatch dispatch = dispatchRepository.findById(idDis).orElseThrow();
        dispatch.removeDepartment(department);
        department.removeDispatch(dispatch);
        Collection<Firefighter> affectedFirefighters = new HashSet<>();
        for (Firefighter firefighter : department.getFirefighters())
            if (dispatch.getAttendees().contains(firefighter)) {
                dispatch.removeAttendee(firefighter);
                affectedFirefighters.add(firefighter);
            }
        firefighterRepository.saveAll(affectedFirefighters);
        repository.save(department);
        dispatchRepository.save(dispatch);
    }

    public void moveFirefighter(Long idDep, Long idFF) throws NoSuchElementException {
        Department department = findOrThrow(idDep);
        Firefighter firefighter = firefighterService.findOrThrow(idFF);
        if (firefighter.getDepartment() != null) {
            firefighter.getDepartment().removeFirefighter(firefighter); // Remove firefighter from previous owner
            repository.save(firefighter.getDepartment());
        }
        firefighter.setDepartment(department);
        department.addFirefighter(firefighter);
        firefighterRepository.save(firefighter);
        repository.save(department);
    }

    public Firefighter newFirefighter(Long id, Firefighter e) throws IllegalArgumentException, NoSuchElementException {
        Department department = findOrThrow(id);

        e.setDepartment(department);
        Firefighter ret = firefighterService.create(e);

        department.addFirefighter(e);

        repository.save(department);
        return ret;
    }

    @Override
    public void update(Department e) throws NoSuchElementException, IllegalArgumentException {
        validate(e);
        Department oldDepartment = findOrThrow(e.getId());
        // could be improved
        oldDepartment.getFirefighters().forEach(e::addFirefighter);
        oldDepartment.getFiretrucks().forEach(e::addFiretruck);
        oldDepartment.getDispatches().forEach(e::addDispatch);
        super.update(e);
    }

    @Override
    public void deleteById(Long id) throws NoSuchElementException {
        Department department = findOrThrow(id);
        firefighterService.deleteAll(department.getFirefighters());

        firetruckService.deleteAll(department.getFiretrucks());

        department.getDispatches().forEach(dispatch -> dispatch.removeDepartment(department));
        dispatchRepository.saveAll(department.getDispatches());

        super.deleteById(id);
    }

    @Override
    protected void validate(Department department) throws IllegalArgumentException {
        if (department.getName() == null || department.getLevel() == null
        || department.getLevel() < 1 || department.getLevel() > 6
        || department.getName().length() <= 3 || department.getName().length() > 255)
            throw new IllegalArgumentException();
    }
}

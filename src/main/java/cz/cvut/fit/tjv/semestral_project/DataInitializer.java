package cz.cvut.fit.tjv.semestral_project;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import cz.cvut.fit.tjv.semestral_project.domain.enums.FiretruckType;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import cz.cvut.fit.tjv.semestral_project.repository.DispatchRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FirefighterRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FiretruckRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

@Component
@Profile("!test")
public class DataInitializer {
    private final JdbcTemplate jdbcTemplate;
    private final DepartmentRepository departmentRepository;
    private final DispatchRepository dispatchRepository;
    private final FirefighterRepository firefighterRepository;
    private final FiretruckRepository firetruckRepository;

    public DataInitializer(JdbcTemplate jdbcTemplate,
                           DepartmentRepository departmentRepository,
                           DispatchRepository dispatchRepository,
                           FirefighterRepository firefighterRepository,
                           FiretruckRepository firetruckRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.departmentRepository = departmentRepository;
        this.dispatchRepository = dispatchRepository;
        this.firefighterRepository = firefighterRepository;
        this.firetruckRepository = firetruckRepository;
    }

    @PostConstruct
    public void initializeData() {
//        clearDB();
//
//        resetSequence("fire_truck_seq");
//        resetSequence("dispatch_seq");
//        resetSequence("firefighter_seq");
//        resetSequence("department_seq");

        Department department1 = createDepartment(1, "Turnov");
        Department department2 = createDepartment(3, "Karlovice");
        Department department3 = createDepartment(5, "Mírová pod Kozákovem");

        LocalDate now = LocalDate.now();
        Random random = new Random();

        int minAge = 18, maxAge = 60;

        Firefighter firefighter1 = createFirefighter("+420666777123", "Tomáš Novák", now.minusYears(random.nextInt(maxAge - minAge) + minAge));
        Firefighter firefighter2 = createFirefighter("666888999", "Pavel Marek", now.minusYears(random.nextInt(maxAge - minAge) + minAge));
        Firefighter firefighter3 = createFirefighter(null, "Jiří Kovář", now.minusYears(random.nextInt(maxAge - minAge) + minAge));
        Firefighter firefighter4 = createFirefighter("1234", "Martin Procházka", now.minusYears(random.nextInt(maxAge - minAge) + minAge));
        Firefighter firefighter5 = createFirefighter(null, "Adam Svoboda", now.minusYears(random.nextInt(maxAge - minAge) + minAge));

        // Assign firefighters to departments
        add(department1, firefighter1);
        add(department1, firefighter2);
        add(department2, firefighter3);
        add(department2, firefighter4);
        add(department3, firefighter5);

        Firetruck firetruck1 = createFiretruck(FiretruckType.DA, "Máňa 1");
        Firetruck firetruck2 = createFiretruck(FiretruckType.CISTERNA, "RTO");
        Firetruck firetruck3 = createFiretruck(FiretruckType.OSOBNI, null);
        Firetruck firetruck4 = createFiretruck(FiretruckType.CISTERNA, "Máňa 2");

        // Assign fire truck to a department
        add(department1, firetruck1);
        add(department3, firetruck2);
        add(department2, firetruck3);
        add(department2, firetruck4);

        Dispatch dispatch1 = createDispatch(1, "Požár", LocalDateTime.now());
        Dispatch dispatch2 = createDispatch(null, "Nehoda", LocalDateTime.now());
        Dispatch dispatch3 = createDispatch(3, "Spadlý strom", LocalDateTime.now());

        // Add departments to the dispatch
        add(department1, dispatch1);
        add(department1, dispatch2);
        add(department1, dispatch3);
        add(department2, dispatch1);
        add(department2, dispatch2);
        add(department3, dispatch1);
        add(department3, dispatch3);

        // Save to the repo
        departmentRepository.save(department1);
        departmentRepository.save(department2);
        departmentRepository.save(department3);

        firefighterRepository.save(firefighter1);
        firefighterRepository.save(firefighter2);
        firefighterRepository.save(firefighter3);
        firefighterRepository.save(firefighter4);
        firefighterRepository.save(firefighter5);

        firetruckRepository.save(firetruck1);
        firetruckRepository.save(firetruck2);
        firetruckRepository.save(firetruck3);
        firetruckRepository.save(firetruck4);

        dispatchRepository.save(dispatch1);
        dispatchRepository.save(dispatch2);
        dispatchRepository.save(dispatch3);
    }

    private void clearDB() {
        jdbcTemplate.execute("TRUNCATE TABLE department, dispatch, dispatch_attendees, dispatch_departments, fire_truck, firefighter, firefighter_dispatches");
    }

    private void resetSequence(String sequenceName) {
        jdbcTemplate.execute("ALTER SEQUENCE " + sequenceName + " RESTART");
    }

    private Department createDepartment(Integer level, String name) {
        Department department = new Department();
        department.setLevel(level);
        department.setName(name);
        return departmentRepository.save(department);
    }

    private Firefighter createFirefighter(String phoneNumber, String fullName, LocalDate birthDate) {
        Firefighter firefighter = new Firefighter();
        firefighter.setPhoneNumber(phoneNumber);
        firefighter.setName(fullName);
        firefighter.setBirthDate(birthDate);
        return firefighterRepository.save(firefighter);
    }

    private Firetruck createFiretruck(FiretruckType type, String name) {
        Firetruck firetruck = new Firetruck();
        firetruck.setType(type);
        firetruck.setName(name);
        return firetruckRepository.save(firetruck);
    }

    private Dispatch createDispatch(Integer dangerLevel, String type, LocalDateTime dispatchStart) {
        Dispatch dispatch = new Dispatch();
        if (dangerLevel != null) dispatch.setDangerLevel(dangerLevel);
        dispatch.setType(type);
        dispatch.setDispatchStart(dispatchStart);
        return dispatchRepository.save(dispatch);
    }

    private void add(Department dep, Firetruck ft) {
        dep.addFiretruck(ft);
        ft.setDepartment(dep);
    }

    private void add(Department dep, Dispatch dis) {
        dep.addDispatch(dis);
        dis.addDepartment(dep);
    }

    private void add(Department dep, Firefighter ff) {
        dep.addFirefighter(ff);
        ff.setDepartment(dep);
    }
}

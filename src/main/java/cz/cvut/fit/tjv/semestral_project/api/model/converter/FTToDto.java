package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.FiretruckDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FTToDto implements Function<Firetruck, FiretruckDto> {
    @Override
    public FiretruckDto apply(Firetruck firetruck) {
        FiretruckDto firetruckDto = new FiretruckDto();
        firetruckDto.setId(firetruck.getId());
        firetruckDto.setName(firetruck.getName());
        firetruckDto.setType(firetruck.getType());
        return firetruckDto;
    }
}

package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.FiretruckDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FTToEntity implements Function<FiretruckDto, Firetruck> {
    @Override
    public Firetruck apply(FiretruckDto firetruckDto) {
        Firetruck firetruck = new Firetruck();
        firetruck.setId(firetruckDto.getId());
        firetruck.setName(firetruckDto.getName());
        firetruck.setType(firetruckDto.getType());
        return firetruck;
    }
}

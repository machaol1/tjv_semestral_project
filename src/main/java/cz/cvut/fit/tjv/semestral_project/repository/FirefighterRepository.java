package cz.cvut.fit.tjv.semestral_project.repository;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FirefighterRepository extends CrudRepository<Firefighter, Long> {
}

package cz.cvut.fit.tjv.semestral_project.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Entity
public class Dispatch extends EntityWithId<Long> {
    private Integer dangerLevel;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String dispatchStart;
    @ManyToMany
    private Set<Department> departments = new HashSet<>();
    @ManyToMany
    private Set<Firefighter> attendees = new HashSet<>();

    public Integer getDangerLevel() {
        return dangerLevel;
    }

    public void setDangerLevel(Integer maxDangerLevel) {
        this.dangerLevel = maxDangerLevel;
    }

    public LocalDateTime getDispatchStart() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        return LocalDateTime.parse(dispatchStart, formatter);
    }

    public void setDispatchStart(LocalDateTime dispatchStart) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        this.dispatchStart = dispatchStart.format(formatter);
    }

    public Collection<Department> getDepartments() {
        return Collections.unmodifiableCollection(departments);
    }

    public void addDepartment(Department department) {
        departments.add(Objects.requireNonNull(department));
    }

    public void removeDepartment(Department department) {
        departments.remove(department);
    }

    public Collection<Firefighter> getAttendees() {
        return Collections.unmodifiableCollection(attendees);
    }

    public void addAttendee(Firefighter attendee) {
        attendees.add(Objects.requireNonNull(attendee));
    }

    public void removeAttendee(Firefighter attendee) {
        attendees.remove(attendee);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Dispatch{" +
                "id=" + id +
                ", dangerLevel=" + dangerLevel +
                ", type" + type +
                ", start=" + dispatchStart +
                ", departments=" + departments +
                ", attendees=" + attendees +
                '}';
    }
}

package cz.cvut.fit.tjv.semestral_project.api;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.api.model.DispatchDto;
import cz.cvut.fit.tjv.semestral_project.api.model.FirefighterDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DepToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DisToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DisToEntity;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.FFToDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.service.DispatchService;
import cz.cvut.fit.tjv.semestral_project.service.FirefighterCannotAttendDispatchHisDepartmentDidNotException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/dispatch")
public class DispatchController extends AbstractCrudController<Dispatch, DispatchDto, Long> {
    FFToDto ffToDto;
    DepToDto depToDto;

    public DispatchController(FFToDto ffToDto, DepToDto depToDto,
                              DispatchService service, DisToDto disToDto, DisToEntity disToEntity) {
        super(service, disToDto, disToEntity);
        this.ffToDto = ffToDto;
        this.depToDto = depToDto;
    }

    @Operation(summary = "Reads all Firefighters that attended Dispatch id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Firefighter successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/firefighter")
    public ResponseEntity<Collection<FirefighterDto>> readAllFirefighters(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<FirefighterDto>>>map(
                dispatch -> ResponseEntity.ok(dispatch.getAttendees().stream().map(ffToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Adds Firefighters idFF to Dispatch idDis")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firefighters successfully added"
            ),
            @ApiResponse(
                responseCode = "400",
                description = "Firefighter cannot attend this Dispatch because his department did not"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFF or idDis is invalid"
            )
        }
    )
    @PutMapping("/{idDis}/firefighter/{idFF}")
    public ResponseEntity<Void> addFirefighter(@PathVariable Long idDis, @PathVariable Long idFF) {
        try {
            ((DispatchService) service).addAttendee(idDis, idFF);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        } catch (FirefighterCannotAttendDispatchHisDepartmentDidNotException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Removes Firefighter idFF from Dispatch idDis")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firefighter successfully removed"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFF or idDis is invalid"
            )
        }
    )
    @DeleteMapping("/{idDis}/firefighter/{idFF}")
    public ResponseEntity<Void> removeFirefighter(@PathVariable Long idDis, @PathVariable Long idFF) {
        try {
            ((DispatchService) service).removeAttendee(idDis, idFF);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Reads all Departments that attended Dispatch id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Departments successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/department")
    public ResponseEntity<Collection<DepartmentDto>> readAllDepartments(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<DepartmentDto>>>map(
                dispatch -> ResponseEntity.ok(dispatch.getDepartments().stream().map(depToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Adds Department idDep to dispatch idDis")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Department successfully added"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDep or idDis is invalid",
                content = @Content
            )
        }
    )
    @PutMapping("/{idDis}/department/{idDep}")
    public ResponseEntity<Void> addDepartments(@PathVariable Long idDis, @PathVariable Long idDep) {
        try {
            ((DispatchService) service).addDepartment(idDis, idDep);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Removes Department idDep from Dispatch idDis")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Department successfully removed"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDep or idDis is invalid"
            )
        }
    )
    @DeleteMapping("/{idDis}/department/{idDep}")
    public ResponseEntity<Void> removeDepartment(@PathVariable Long idDis, @PathVariable Long idDep) {
        try {
            ((DispatchService) service).removeDepartment(idDis, idDep);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }
}

package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.FirefighterDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FFToEntity implements Function<FirefighterDto, Firefighter> {
    @Override
    public Firefighter apply(FirefighterDto firefighterDto) {
        Firefighter firefighter = new Firefighter();
        firefighter.setId(firefighterDto.getId());
        firefighter.setName(firefighterDto.getName());
        firefighter.setPhoneNumber(firefighterDto.getPhoneNumber());
        firefighter.setBirthDate(firefighterDto.getBirthDate());
        return firefighter;
    }
}

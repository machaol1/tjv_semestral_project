package cz.cvut.fit.tjv.semestral_project.api;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.api.model.DispatchDto;
import cz.cvut.fit.tjv.semestral_project.api.model.FirefighterDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DepToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DisToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.FFToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.FFToEntity;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.service.FirefighterCannotAttendDispatchHisDepartmentDidNotException;
import cz.cvut.fit.tjv.semestral_project.service.FirefighterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/firefighter")
public class FirefighterController extends AbstractCrudController<Firefighter, FirefighterDto, Long> {
    DisToDto disToDto;
    DepToDto depToDto;

    public FirefighterController(DisToDto disToDto, DepToDto depToDto,
                                 FirefighterService service, FFToDto FFToDtoConverter, FFToEntity FFToEntityConverter) {
        super(service, FFToDtoConverter, FFToEntityConverter);
        this.disToDto = disToDto;
        this.depToDto = depToDto;
    }

    @Operation(summary = "Reads all Dispatches Firefighter id has attended")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Dispatches successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/dispatch")
    public ResponseEntity<Collection<DispatchDto>> readAllDispatches(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<DispatchDto>>>map(
                firefighter -> ResponseEntity.ok(firefighter.getDispatches().stream().map(disToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Adds Dispatch idDis from Firefighters idFF attended Dispatches")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firefighter successfully added"
            ),
            @ApiResponse(
                responseCode = "400",
                description = "Firefighter cannot attend Dispatch his Department did not"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFF or idDis is invalid"
            )
        }
    )
    @PutMapping("/{idFF}/dispatch/{idDis}")
    public ResponseEntity<Void> addAttend(@PathVariable Long idFF, @PathVariable Long idDis) {
        try {
            ((FirefighterService) service).attend(idFF, idDis);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        } catch (FirefighterCannotAttendDispatchHisDepartmentDidNotException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Removes Dispatch idDis from Firefighters idFF attended Dispatches")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Entity successfully updated"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDis or idFF is invalid"
            )
        }
    )
    @DeleteMapping("/{idFF}/dispatch/{idDis}")
    public ResponseEntity<Void> removeAttend(@PathVariable Long idFF, @PathVariable Long idDis) {
        try {
            ((FirefighterService) service).removeAttend(idFF, idDis);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Reads Firefighters id Department")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Department successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/department")
    public ResponseEntity<DepartmentDto> readDepartment(@PathVariable Long id) {
        return service.readById(id).map(
                firefighter -> ResponseEntity.ok(depToDto.apply(firefighter.getDepartment()))
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Sets Firefighters idFF Department idDep and removes Firefighter from his original Department")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firefighter successfully moved"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFF or idDep is invalid"
            )
        }
    )
    @PutMapping("/{idFF}/department/{idDep}")
    public ResponseEntity<Void> setDepartment(@PathVariable Long idFF, @PathVariable Long idDep) {
        try {
            ((FirefighterService) service).setDepartment(idFF, idDep);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @Operation(summary = "Forbidden way of creating Firefighter")
    @ApiResponse(
        responseCode = "405",
        content = @Content
    )
    public ResponseEntity<FirefighterDto> create(FirefighterDto e) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }
}

package cz.cvut.fit.tjv.semestral_project.api;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.api.model.FiretruckDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DepToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.FTToDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.FTToEntity;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import cz.cvut.fit.tjv.semestral_project.service.FiretruckService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping("/firetruck")
public class FiretruckController extends AbstractCrudController<Firetruck, FiretruckDto, Long> {
    DepToDto depToDto;

    public FiretruckController(DepToDto depToDto, FiretruckService service,
                               FTToDto FTToDto, FTToEntity FTToEntity) {
        super(service, FTToDto, FTToEntity);
        this.depToDto = depToDto;
    }

    @Operation(summary = "Reads Firetrucks id Department")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Departments successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/department")
    public ResponseEntity<DepartmentDto> readDepartment(@PathVariable Long id) {
        return service.readById(id).map(
                firetruck -> ResponseEntity.ok(depToDto.apply(firetruck.getDepartment()))
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Changes Firetrucks idFT to Department idDep and removed it from the original Department")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Department successfully changed"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFT or idDep is invalid"
            )
        }
    )
    @PutMapping("/{idFT}/department/{idDep}")
    public ResponseEntity<Void> setDepartment(@PathVariable Long idFT, @PathVariable Long idDep) {
        try {
            ((FiretruckService) service).setDepartment(idFT, idDep);
            return ResponseEntity.ok().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    @Operation(summary = "Forbidden way of creating Firetruck")
    @ApiResponse(
        responseCode = "405",
        content = @Content
    )
    public ResponseEntity<FiretruckDto> create(FiretruckDto e) {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
    }
}

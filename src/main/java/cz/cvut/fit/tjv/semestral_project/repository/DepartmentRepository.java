package cz.cvut.fit.tjv.semestral_project.repository;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {
}

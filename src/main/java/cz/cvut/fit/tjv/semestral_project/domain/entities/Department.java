package cz.cvut.fit.tjv.semestral_project.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;

import java.util.*;

@Entity
public class Department extends EntityWithId<Long> {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer level;
    @OneToMany(mappedBy = "department")
    private Set<Firefighter> firefighters = new HashSet<>();
    @OneToMany(mappedBy = "department")
    private Set<Firetruck> firetrucks = new HashSet<>();
    @ManyToMany(mappedBy = "departments")
    private Set<Dispatch> dispatches = new HashSet<>();

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Firefighter> getFirefighters() {
        return Collections.unmodifiableCollection(firefighters);
    }

    public void addFirefighter(Firefighter firefighter) {
        firefighters.add(Objects.requireNonNull(firefighter));
    }

    public void removeFirefighter(Firefighter firefighter) {
        firefighters.remove(firefighter);
    }

    public Collection<Firetruck> getFiretrucks() {
        return Collections.unmodifiableCollection(firetrucks);
    }

    public void addFiretruck(Firetruck firetruck) {
        firetrucks.add(Objects.requireNonNull(firetruck));
    }

    public void removeFiretruck(Firetruck firetruck) {
        firetrucks.remove(firetruck);
    }

    public Collection<Dispatch> getDispatches() {
        return Collections.unmodifiableCollection(dispatches);
    }

    public void addDispatch(Dispatch dispatch) {
        dispatches.add(Objects.requireNonNull(dispatch));
    }

    public void removeDispatch(Dispatch dispatch) {
        dispatches.remove(dispatch);
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name=" + name +
                ", level=" + level +
                ", firefighters=" + firefighters +
                ", firetrucks=" + firetrucks +
                ", dispatches=" + dispatches +
                '}';
    }
}

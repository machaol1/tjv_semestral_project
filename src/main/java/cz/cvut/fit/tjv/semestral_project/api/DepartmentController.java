package cz.cvut.fit.tjv.semestral_project.api;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.api.model.DispatchDto;
import cz.cvut.fit.tjv.semestral_project.api.model.FirefighterDto;
import cz.cvut.fit.tjv.semestral_project.api.model.FiretruckDto;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.*;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.service.DepartmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/department")
public class DepartmentController extends AbstractCrudController<Department, DepartmentDto, Long> {
    FTToDto ftToDto;
    DisToDto disToDto;
    FFToDto ffToDto;
    FTToEntity ftToEntity;
    FFToEntity ffToEntity;

    public DepartmentController(FTToDto ftToDto, DisToDto disToDto, FFToDto ffToDto,
                                FTToEntity ftToEntity, FFToEntity ffToEntity,
                                DepartmentService service, DepToDto depToDto, DepToEntity depToEntity) {
        super(service, depToDto, depToEntity);
        this.ftToDto = ftToDto;
        this.disToDto = disToDto;
        this.ffToDto = ffToDto;
        this.ftToEntity = ftToEntity;
        this.ffToEntity = ffToEntity;
    }

    @Operation(summary = "Read all Firetrucks that belong to Department id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Firetrucks successfully sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/firetruck")
    public ResponseEntity<Collection<FiretruckDto>> readAllFiretrucks(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<FiretruckDto>>>map(
                department -> ResponseEntity.ok(department.getFiretrucks().stream().map(ftToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Moves Firetruck idFT to Department idDep")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firetruck successfully moved"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDep or idFT is invalid"
            )
        }
    )
    @PutMapping("/{idDep}/firetruck/{idFT}")
    public ResponseEntity<Void> moveFiretruck(@PathVariable Long idDep, @PathVariable Long idFT) {
        try {
            ((DepartmentService) service).moveFiretruck(idDep, idFT);
            return ResponseEntity.noContent().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Create new Firetruck that belongs to Department id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Firetruck successfully saved"
            ),
            @ApiResponse(
                responseCode = "400",
                description = "Provided Firetruck is invalid",
                content = @Content
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid",
                content = @Content
            )
        }
    )
    @PostMapping("/{id}/firetruck")
    public ResponseEntity<FiretruckDto> newFiretruck(@PathVariable Long id, @RequestBody FiretruckDto firetruckDto) {
        try {
            return ResponseEntity.ok(ftToDto.apply(
                    ((DepartmentService) service).newFiretruck(id, ftToEntity.apply(firetruckDto))
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Read all Dispatches this Department id attended")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Dispatches successfully read"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Department id is invalid",
                content = @Content
            )
        }
    )
    @GetMapping("/{id}/dispatch")
    public ResponseEntity<Collection<DispatchDto>> readAllDispatches(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<DispatchDto>>>map(
                department -> ResponseEntity.ok(department.getDispatches().stream().map(disToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Adds Dispatch idDis to Department idDep")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Dispatch successfully added"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDep or idDis is invalid"
            )
        }
    )
    @PutMapping("/{idDep}/dispatch/{idDis}")
    public ResponseEntity<Void> addDispatch(@PathVariable Long idDep, @PathVariable Long idDis) {
        try {
            ((DepartmentService) service).addDispatch(idDep, idDis);
            return ResponseEntity.noContent().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Removes Dispatch idDis from Department idDep")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Dispatch successfully removed"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idDis or idDep is invalid"
            ),
        }
    )
    @DeleteMapping("/{idDep}/dispatch/{idDis}")
    public ResponseEntity<Void> removeDispatch(@PathVariable Long idDep, @PathVariable Long idDis) {
        try {
            ((DepartmentService) service).removeDispatch(idDep, idDis);
            return ResponseEntity.noContent().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Read all Firefighters that belong to Department id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Firefighters got sent"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided id is invalid"
            )
        }
    )
    @GetMapping("/{id}/firefighter")
    public ResponseEntity<Collection<FirefighterDto>> readAllFirefighters(@PathVariable Long id) {
        return service.readById(id).<ResponseEntity<Collection<FirefighterDto>>>map(
                department -> ResponseEntity.ok(department.getFirefighters().stream().map(ffToDto).toList())
                ).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Move Firefighter idFF to Department idDep")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "204",
                description = "Firefighter successfully moved"
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Provided idFF or idDep is invalid"
            )
        }
    )
    @PutMapping("/{idDep}/firefighter/{idFF}")
    public ResponseEntity<Void> moveFirefighter(@PathVariable Long idDep, @PathVariable Long idFF) {
        try {
            ((DepartmentService) service).moveFirefighter(idDep, idFF);
            return ResponseEntity.noContent().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Create new Firefighter that belongs to Department id")
    @ApiResponses(
        value = {
            @ApiResponse(
                responseCode = "200",
                description = "Firefighter successfully saved",
                content = {
                    @Content(
                        mediaType = "application/json",
                        schema = @Schema(implementation = FirefighterDto.class)
                    )
                }
            ),
            @ApiResponse(
                responseCode = "400",
                description = "Provided Firefighter is invalid (firefighter also must be 18+)",
                content = @Content
            ),
            @ApiResponse(
                responseCode = "404",
                description = "Department id is invalid",
                content = @Content
            )
        }
    )
    @PostMapping("/{id}/firefighter")
    public ResponseEntity<FirefighterDto> newFirefighter(@PathVariable Long id,
                                                         @RequestBody FirefighterDto firefighterDto) {
        try {
            return ResponseEntity.ok(ffToDto.apply(
                    ((DepartmentService) service).newFirefighter(id, ffToEntity.apply(firefighterDto))
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }
}

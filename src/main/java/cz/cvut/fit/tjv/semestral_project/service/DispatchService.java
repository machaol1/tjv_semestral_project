package cz.cvut.fit.tjv.semestral_project.service;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import cz.cvut.fit.tjv.semestral_project.repository.DispatchRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FirefighterRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class DispatchService extends AbstractCrudService<Dispatch, Long> {
    private final DepartmentRepository departmentRepository;
    private final FirefighterRepository firefighterRepository;

    public DispatchService(DispatchRepository dispatchRepository,
                           DepartmentRepository departmentRepository,
                           FirefighterRepository firefighterRepository) {
        super(dispatchRepository);
        this.departmentRepository = departmentRepository;
        this.firefighterRepository = firefighterRepository;
    }

    public void addAttendee(Long idDis, Long idFF) throws NoSuchElementException,
            FirefighterCannotAttendDispatchHisDepartmentDidNotException {
        Dispatch dispatch = findOrThrow(idDis);
        Firefighter firefighter = firefighterRepository.findById(idFF).orElseThrow();
        if (!dispatch.getDepartments().contains(firefighter.getDepartment()))
            throw new FirefighterCannotAttendDispatchHisDepartmentDidNotException();

        firefighter.addDispatch(dispatch);
        dispatch.addAttendee(firefighter);

        repository.save(dispatch);
        firefighterRepository.save(firefighter);
    }

    public void removeAttendee(Long idDis, Long idFF) throws NoSuchElementException {
        Dispatch dispatch = findOrThrow(idDis);
        Firefighter firefighter = firefighterRepository.findById(idFF).orElseThrow();

        firefighter.removeDispatch(dispatch);
        dispatch.removeAttendee(firefighter);

        repository.save(dispatch);
        firefighterRepository.save(firefighter);
    }

    public void addDepartment(Long idDis, Long idDep) throws NoSuchElementException {
        Dispatch dispatch = findOrThrow(idDis);
        Department department = departmentRepository.findById(idDep).orElseThrow();

        dispatch.addDepartment(department);
        department.addDispatch(dispatch);

        repository.save(dispatch);
        departmentRepository.save(department);
    }

    public void removeDepartment(Long idDis, Long idDep) throws NoSuchElementException {
        Dispatch dispatch = findOrThrow(idDis);
        Department department = departmentRepository.findById(idDep).orElseThrow();

        for (Firefighter firefighter : department.getFirefighters())
            firefighter.removeDispatch(dispatch);

        dispatch.removeDepartment(department);
        department.removeDispatch(dispatch);

        firefighterRepository.saveAll(department.getFirefighters());
        repository.save(dispatch);
        departmentRepository.save(department);
    }

    @Override
    public void update(Dispatch e) throws NoSuchElementException, IllegalArgumentException {
        validate(e);
        Dispatch oldDispatch = findOrThrow(e.getId());
        // could be improved
        oldDispatch.getDepartments().forEach(e::addDepartment);
        oldDispatch.getAttendees().forEach(e::addAttendee);
        super.update(e);
    }

    @Override
    public void deleteById(Long id) throws NoSuchElementException {
        Dispatch dispatch = findOrThrow(id);

        dispatch.getAttendees().forEach(firefighter -> firefighter.removeDispatch(dispatch));
        firefighterRepository.saveAll(dispatch.getAttendees());

        // Remove relationships with departments
        dispatch.getDepartments().forEach(department -> department.removeDispatch(dispatch));
        departmentRepository.saveAll(dispatch.getDepartments());

        super.deleteById(id);
    }

    @Override
    protected void validate(Dispatch dispatch) throws IllegalArgumentException {
        if (dispatch.getDispatchStart() == null
        || (dispatch.getDangerLevel() != null && (dispatch.getDangerLevel() < 0 || dispatch.getDangerLevel() > 7))
        || dispatch.getType().length() < 3 || dispatch.getType().length() > 255)
            throw new IllegalArgumentException();
    }
}

package cz.cvut.fit.tjv.semestral_project.api.model.converter;

import cz.cvut.fit.tjv.semestral_project.api.model.DepartmentDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component("DepToDto")
public class DepToDto implements Function<Department, DepartmentDto> {
    @Override
    public DepartmentDto apply(Department department) {
        if (department == null) return null;
        DepartmentDto aDepartmentDto = new DepartmentDto();
        aDepartmentDto.setId(department.getId());
        aDepartmentDto.setName(department.getName());
        aDepartmentDto.setLevel(department.getLevel());
        return aDepartmentDto;
    }
}

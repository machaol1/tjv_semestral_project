package cz.cvut.fit.tjv.semestral_project.controller;

import cz.cvut.fit.tjv.semestral_project.api.FiretruckController;
import cz.cvut.fit.tjv.semestral_project.api.model.converter.DepToDto;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firetruck;
import cz.cvut.fit.tjv.semestral_project.domain.enums.FiretruckType;
import cz.cvut.fit.tjv.semestral_project.service.FiretruckService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(FiretruckController.class)
@ComponentScan(basePackages = "cz.cvut.fit.tjv.semestral_project.api.model.converter")
@Import({DepToDto.class})
public class FiretruckControllerTest {
    @MockBean
    FiretruckService firetruckService;
    @Autowired
    MockMvc mockMvc;

    @Test
    public void testFindAll() throws Exception {
        Firetruck firetruck1 = new Firetruck();
        firetruck1.setId(1L);
        firetruck1.setDepartment(new Department());
        firetruck1.setType(FiretruckType.CISTERNA);
        firetruck1.setName("Máňa");
        Firetruck firetruck2 = new Firetruck();
        firetruck1.setId(2L);
        firetruck2.setDepartment(new Department());
        firetruck2.setType(FiretruckType.DA);

        List<Firetruck> firetrucks = List.of(firetruck1, firetruck2);

        Mockito.when(firetruckService.readAll()).thenReturn(firetrucks);

        mockMvc.perform(get("/firetruck"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].type", Matchers.is(FiretruckType.CISTERNA.toString())));
    }
}

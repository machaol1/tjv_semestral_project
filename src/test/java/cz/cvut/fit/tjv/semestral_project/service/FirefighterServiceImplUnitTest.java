package cz.cvut.fit.tjv.semestral_project.service;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Dispatch;
import cz.cvut.fit.tjv.semestral_project.domain.entities.Firefighter;
import cz.cvut.fit.tjv.semestral_project.repository.DispatchRepository;
import cz.cvut.fit.tjv.semestral_project.repository.FirefighterRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ActiveProfiles("test")
class FirefighterServiceImplUnitTest {
    @Autowired
    private FirefighterService firefighterService;
    @MockBean
    private FirefighterRepository firefighterRepository;
    @MockBean
    private DispatchRepository dispatchRepository;
    private Firefighter firefighter = new Firefighter();
    private Dispatch dispatch = new Dispatch();

    @BeforeEach
    void setUp() {
        Department department = new Department();
        department.setId(69L);
        department.setLevel(1);

        firefighter.setBirthDate(LocalDate.of(2002, 3, 27));
        firefighter.setId(1L);
        firefighter.setName("test name");
        firefighter.setPhoneNumber("+420666777123");

        dispatch.setDispatchStart(LocalDateTime.now());
        dispatch.setId(42L);
        dispatch.setDangerLevel(2);

        firefighter.setDepartment(department);
        dispatch.addDepartment(department);
        department.addFirefighter(firefighter);
        department.addDispatch(dispatch);

        Mockito.when(firefighterRepository.findById(firefighter.getId())).thenReturn(Optional.of(firefighter));
        Mockito.when(dispatchRepository.findById(dispatch.getId())).thenReturn(Optional.of(dispatch));
    }

    @Test
    void attendInvalidFirefighterId() {
        Mockito.when(firefighterRepository.findById(firefighter.getId())).thenReturn(Optional.empty());

        Assertions.assertThrows(NoSuchElementException.class, () -> firefighterService.attend(firefighter.getId(), dispatch.getId()));

        Mockito.verify(firefighterRepository, Mockito.times(1)).findById(firefighter.getId());
        Mockito.verifyNoMoreInteractions(firefighterRepository);
    }

    @Test
    void attendInvalidDispatchId() {
        Mockito.when(dispatchRepository.findById(dispatch.getId())).thenReturn(Optional.empty());

        Assertions.assertThrows(NoSuchElementException.class, () -> firefighterService.attend(firefighter.getId(), dispatch.getId()));

        Mockito.verify(dispatchRepository, Mockito.times(1)).findById(dispatch.getId());
        Mockito.verifyNoMoreInteractions(dispatchRepository);
    }

    @Test
    void attend() {
        firefighterService.attend(firefighter.getId(), dispatch.getId());

        Mockito.verify(dispatchRepository, Mockito.atLeastOnce()).save(dispatch);
        Mockito.verify(firefighterRepository, Mockito.atLeastOnce()).save(firefighter);
        Assertions.assertTrue(firefighter.getDispatches().contains(dispatch));
        Assertions.assertTrue(dispatch.getAttendees().contains(firefighter));
    }

    @Test
    void attendFirefighterAttendsDispatchHisDepartmentDidNot() {
        dispatch = new Dispatch();
        firefighter = new Firefighter();
        Mockito.when(firefighterRepository.findById(firefighter.getId())).thenReturn(Optional.of(firefighter));
        Mockito.when(dispatchRepository.findById(dispatch.getId())).thenReturn(Optional.of(dispatch));

        Assertions.assertThrows(FirefighterCannotAttendDispatchHisDepartmentDidNotException.class, () -> firefighterService.attend(firefighter.getId(), dispatch.getId()));
        Mockito.verify(firefighterRepository, Mockito.never()).save(Mockito.any());
        Mockito.verify(dispatchRepository, Mockito.never()).save(Mockito.any());
    }
}

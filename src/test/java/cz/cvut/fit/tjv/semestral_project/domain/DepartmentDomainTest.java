package cz.cvut.fit.tjv.semestral_project.domain;

import cz.cvut.fit.tjv.semestral_project.domain.entities.Department;
import cz.cvut.fit.tjv.semestral_project.repository.DepartmentRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class DepartmentDomainTest {
    @Autowired
    DepartmentRepository departmentRepository;

    @Test
    public void testCreateReadDelete() {
        Department department = new Department();
        department.setName("Turnov 2");
        department.setLevel(2);

        departmentRepository.save(department);

        Iterable<Department> departments = departmentRepository.findAll();
        Assertions.assertThat(departments).extracting(Department::getName).containsOnly("Turnov 2");
        Assertions.assertThat(departments).extracting(Department::getLevel).containsOnly(2);

        departmentRepository.deleteAll();
        Assertions.assertThat(departmentRepository.findAll()).isEmpty();
    }
}
